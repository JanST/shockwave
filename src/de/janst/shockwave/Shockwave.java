package de.janst.shockwave;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Shockwave extends JavaPlugin {

	public void onEnable() {
		
	}
	
	public void onDisable()
	{
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(label.equalsIgnoreCase("shockwave")) {
				int radius = 10;
				if(args.length > 0) {
					radius = Integer.parseInt(args[0]);
				}
				ShockwaveHandler sh = new ShockwaveHandler(this, radius);
				sh.createShockwave(player);
				player.sendMessage("wow...");
				return true;
			}
		}
		else {
			sender.sendMessage("You have to be a Player to do this");
			return true;
		}
		return false;
	}
}
